import jdatetime
import datetime
from prettytable import PrettyTable

YY, MM, DD = ('', '', '')
data = input("Enter date in this format:  YY/MM/DD   Or   MM/DD\n->").strip()

if data.count('/') == 1:
    # MM/DD
    MM, DD = data.split('/')
elif data.count('/') == 2:
    YY, MM, DD = map(lambda x: int(x), data.split('/'))
else:
    print('Error! check your inputs')
    exit()

if not YY:
    YY_g = datetime.datetime.now().year
    YY_j = jdatetime.datetime.now().year
else:
    YY_g, YY_j = YY, YY

MM, DD = int(MM), int(DD)
#print(YY_g, YY_j, MM, DD)
row_1 = jdatetime.date(YY_j,MM,DD).togregorian().isoformat()
row_2 = jdatetime.date.fromgregorian(day=DD,month=MM,year=YY_g).isoformat()
table = PrettyTable(['Format', 'Value'])
table.add_row(['Gregorian', row_1])
table.add_row(['Jalali', row_2])
print(table)
#print('in gregorian ----', jdatetime.date(YY_j,MM,DD).togregorian())
#print('in jalali ----',  jdatetime.date.fromgregorian(day=DD,month=MM,year=YY_g))
