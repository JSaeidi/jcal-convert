# jcal-convert

convert `Gregorian` and `Jalali` to each other.

`$ python3  jcal-convert/main.py`
```
Enter date in this format:  YY/MM/DD   Or   MM/DD
->1399/7/15
```

```
+-----------+------------+
|   Format  |   Value    |
+-----------+------------+
| Gregorian | 2020-10-06 |
|   Jalali  | 778-04-24  |
+-----------+------------+

```
